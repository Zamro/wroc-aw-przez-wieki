package com.example.pawel.photomap;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pawel on 2015-04-22.
 */
public class MapGalleryObjectEntity {
    public Boolean isUnlocked = false;
    public String name = "";
    public String history = "";
    public LatLng position;
    public List<MapGalleryPhoto> photos = new LinkedList<MapGalleryPhoto>();

    MapGalleryObjectEntity()
    {
        position = new LatLng(0,0);
    }

    MapGalleryObjectEntity(String name, String history, LatLng position)
    {
        this.name = name;
        this.history = history;
        this.position= position;
    }

    public void addPhoto(MapGalleryPhoto photo)
    {
        photos.add(photo);
    }
}
