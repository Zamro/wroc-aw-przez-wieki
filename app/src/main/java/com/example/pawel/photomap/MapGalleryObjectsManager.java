package com.example.pawel.photomap;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pawel on 2015-04-22.
 */
public class MapGalleryObjectsManager {
    private static List<MapGalleryObjectEntity> mapGalleryObjects;
    //= new LinkedList<>();

    private static void loadMapObjects(){
        mapGalleryObjects = new LinkedList<>();
        MapGalleryObjectEntity institute = new MapGalleryObjectEntity("Instytut Matematyki", "Historia Instytutu Matematyki", new LatLng(51.110258, 17.053977));
        institute.addPhoto(new MapGalleryPhoto(1975, "http://dolny-slask.org.pl/foto/11/11387.jpg"));
        institute.addPhoto(new MapGalleryPhoto(1973, "http://dolny-slask.org.pl/foto/5410/5410250.jpg"));
        institute.addPhoto(new MapGalleryPhoto(2013, "http://dolny-slask.org.pl/foto/3899/3899477.jpg"));
        institute.addPhoto(new MapGalleryPhoto(2005, "http://dolny-slask.org.pl/foto/395/395794.jpg"));
        institute.isUnlocked = true;
        mapGalleryObjects.add(institute);

        MapGalleryObjectEntity panorama = new MapGalleryObjectEntity("Panorama Racławicka", "Panorama Racławicka – muzeum sztuki we Wrocławiu, oddział Muzeum Narodowego we Wrocławiu, założone w 1893 roku we Lwowie[2], od 1980 roku we Wrocławiu; eksponuje cykloramiczny obraz Bitwa pod Racławicami namalowany w latach 1893–1894 pod kierunkiem Jana Styki i Wojciecha Kossaka. Obraz olejny przedstawia bitwę pod Racławicami (1794), jeden z epizodów insurekcji kościuszkowskiej, zwycięstwo wojsk polskich pod dowództwem gen. Tadeusza Kościuszki nad wojskami rosyjskimi pod dowództwem gen. Aleksandra Tormasowa.", new LatLng(51.11013,17.044331));
        panorama .addPhoto(new MapGalleryPhoto(2005, "http://upload.wikimedia.org/wikipedia/commons/3/3e/RotundaPanoramyRaclawickiej.jpg"));
        mapGalleryObjects.add(panorama );
    }

    public static List<MapGalleryObjectEntity> getObjects(){
        if (mapGalleryObjects == null)
            loadMapObjects();
        return mapGalleryObjects;
    }

    public static MapGalleryObjectEntity getObject(String title) {
        for(MapGalleryObjectEntity object : getObjects())
            if(object.name.equals(title))
                return object;
        return null;
    }
}
