package com.example.pawel.photomap;

import java.util.Date;

/**
 * Created by Pawel on 2015-04-22.
 */
public class MapGalleryPhoto {
    //String title; ?
    int year;
    String url;
    MapGalleryPhoto(int year, String url){
        this.year = year;
        this.url = url;
    }
}
