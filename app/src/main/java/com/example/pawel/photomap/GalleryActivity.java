package com.example.pawel.photomap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class GalleryActivity extends ActionBarActivity {
    public MapGalleryObjectEntity mapGalleryObjectEntity;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Intent i = getIntent();
        mapGalleryObjectEntity = MapGalleryObjectsManager.getObject(i.getStringExtra("title"));

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
//            return PlaceholderFragment.newInstance(position + 1);
            if(position==0)
                return new HistoryFragment();
            return new ImageFragment(position);
//            return ImageFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return mapGalleryObjectEntity.photos.size() + 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0)
                return "Historia";
            return Integer.toString(mapGalleryObjectEntity.photos.get(position+1).year);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public class HistoryFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
//        public static HistoryFragment newInstance(int sectionNumber) {
//            HistoryFragment fragment = new HistoryFragment();
//            return fragment;
//        }

        public HistoryFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.history_fragment, container, false);
            TextView textView = (TextView)rootView.findViewById(R.id.history);
            textView.setText(mapGalleryObjectEntity.history);
            return rootView;
        }
    }


    public class ImageFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
//        public static HistoryFragment newInstance(int sectionNumber) {
//            HistoryFragment fragment = new HistoryFragment();
//            return fragment;
//        }
        String url;

        public ImageFragment(int position) {
            url = mapGalleryObjectEntity.photos.get(position-1).url;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.image_fragment, container, false);

            new Thread(){
                @Override
                public void run() {
                    try {
                        final ImageView imageView = (ImageView)rootView.findViewById(R.id.image);
                        //            imageView.setImageURI(Uriurl);
                        final Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
                        runOnUiThread( new Runnable(){
                            @Override
                            public void run() {
                                imageView.setImageBitmap(bitmap);
                            }
                        });
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }.start();

            TextView textView = (TextView)rootView.findViewById(R.id.url);
            textView.setText(url);

            return rootView;
        }
    }

}
